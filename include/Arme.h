#ifndef ARME_H
#define ARME_H

#include <iostream>
#include <string>


class Arme
{
    public:
        Arme();
        Arme(std::string nomPrecis , int degatsPrecis);
        void changer(std::string nom2, int degats2);
        void afficher();
        int getDegats();


    private:
        std::string nom;
        int degats;
};

#endif // ARME_H
