#ifndef PERSONNAGE_H
#define PERSONNAGE_H

#include <iostream>
#include <string>
#include "Arme.h"

class Personnage
{
  private:
        int vie;
        int mana;
        Arme armePersonnage;
        std::string nom;


  public:
       Personnage();
       Personnage(std::string nomArmeAmeliore, int degatsArmeAmeliore);
      // Personnage(int pointsVie, int pointsMana);
       ~Personnage();
       void recevoirDegats(int nbDegats);
       void attaquer(Personnage &cible);
       void boirePotionDeVie(int quantitePotion);
       void changerArme(std::string nomNouvelleArme , int degatsNouvelleArme);
       bool estVivant();
       void afficherEtat();
       std::string getNom();
       void attaqueMagique(Personnage &cible);
};

#endif // PERSONNAGE_H
