#include <iostream>
#include <string>
#include <cmath>
#include <vector>
#include <fstream>
#include <ctime>
#include <cstdlib>
#include "math.h"
#include "Personnage.h"
#include "Arme.h"

using namespace std;


int main()
{
    Personnage david, goliath("�p�e aiguis�e", 20) , zoro("3 sabres" , 12);

    //Au combat !
    goliath.attaquer(david);
    david.boirePotionDeVie(20);
    goliath.attaquer(david);
    goliath.attaqueMagique(david);
    david.attaquer(goliath);
    david.attaqueMagique(goliath);
    zoro.attaquer(goliath);
    zoro.attaqueMagique(goliath);
    goliath.changerArme("Double hache tranchante v�n�neuse de la mort", 40);
    goliath.attaquer(david);
    goliath.attaqueMagique(david);
    zoro.attaquer(goliath);
    zoro.attaqueMagique(goliath);

    cout << endl;
    david.afficherEtat();
    cout << endl;
    goliath.afficherEtat();
    cout << endl;
    zoro.afficherEtat();
    cout << endl;

    goliath.attaquer(david);
    goliath.attaqueMagique(david);
    david.attaquer(goliath);
    david.attaqueMagique(goliath);
    zoro.attaquer(goliath);
    zoro.attaqueMagique(goliath);

    cout << endl;
    david.afficherEtat();
    cout << endl;
    goliath.afficherEtat();
    cout << endl;
    zoro.afficherEtat();
    cout << endl;

    goliath.attaquer(david);
    goliath.attaqueMagique(david);
    david.attaquer(goliath);
    david.attaqueMagique(goliath);
    zoro.attaquer(goliath);
    zoro.attaqueMagique(goliath);

    cout << endl;
    david.afficherEtat();
    cout << endl;
    goliath.afficherEtat();
    cout << endl;
    zoro.afficherEtat();
    cout << endl;

    david.estVivant();
    goliath.estVivant();
    zoro.estVivant();


    cout << endl;
    cout << endl;
    cout << endl;
    int choixAction;
    cout << "Choisisser une action pour " << david.getNom() << endl;
    cout << "1) Attaquer " << zoro.getNom() << " avec l'arme." << endl;
    cout << "2) Attaquer " << zoro.getNom() << " avec un sort." << endl;
    cout << "3) Boire une potion de vie." << endl;
    cout << endl;
    cout << "Votre choix : ";
    cin >> choixAction;
    switch(choixAction)
    {
    case 1:
       goliath.attaquer(zoro);
       break;
    case 2:
       david.attaqueMagique(zoro);
       break;
    case 3:
       zoro.boirePotionDeVie(40);
       break;
    default:
       printf("Vous n'avez pas rentre un nombre correct.");
       break;
  }
    cout << endl;
    return 0;
}


