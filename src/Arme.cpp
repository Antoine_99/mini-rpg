#include "Arme.h"

using namespace std;

Arme::Arme()
{
    nom = "�p�e rouill�e";
    degats = 10;
}

Arme::Arme(string nomPrecis , int degatsPrecis)
{
    nom = nomPrecis;
    degats = degatsPrecis;
}

void Arme::changer(string nom2, int degats2)
{
    nom = nom2;
    degats = degats2;
}

void Arme::afficher()
{
    cout << "Nom de l'arme : " << nom  << " (D�g�ts : " << degats << ")" << endl;
}

int Arme::getDegats()
{
   return degats;
}
