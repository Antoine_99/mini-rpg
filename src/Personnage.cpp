#include "Personnage.h"
#include "Arme.h"

using namespace std;

Personnage::Personnage()
{
     vie = 100;
     mana = 100;
     cout << "Nom du personnage : ";
     cin >> nom;
      if(nom.size() < 4)
     {
         nom = "ordi";
     }
     cout << endl;
}

Personnage::Personnage(string nomArmeAmeliore, int degatsArmeAmeliore) : vie(100) , mana (100) , armePersonnage(nomArmeAmeliore, degatsArmeAmeliore)
{
    cout << "Nom du personnage : ";
    cin >> nom;
     if(nom.size() < 4)
     {
         nom = "ordi";
     }
    cout << endl;
}

/*Personnage::Personnage(int pointsVie, int pointsMana)
{
     vie = pointsVie;
     mana = pointsMana;
     nomArme = "�p�e rouill�e";
     degatsArme = 10;
} */

Personnage::~Personnage()
{

}

void Personnage::recevoirDegats(int nbDegats)
{
    vie -= nbDegats;
    //On enl�ve le nombre de d�g�ts re�us � la vie du personnage

    if (vie < 0) //Pour �viter d'avoir une vie n�gative
    {
        vie = 0; //On met la vie � 0 (cela veut dire mort)
    }

    cout << nom << " re�oit " << nbDegats << " points de d�gats." << endl;
}

void Personnage::attaquer(Personnage &cible)
{
    cout << nom << " attaque " << cible.getNom() << endl;
    cible.recevoirDegats(armePersonnage.getDegats());
}

void Personnage::boirePotionDeVie(int quantitePotion)
{
    vie += quantitePotion;

    if(vie > 100)
    {
        vie = 100;
    }
    cout << nom << " boit une potion de vie et se soigne de " << quantitePotion << " points de vie." << endl;
}

void Personnage::changerArme(string nomNouvelleArme , int degatsNouvelleArme)
{
    armePersonnage.changer(nomNouvelleArme , degatsNouvelleArme);
    cout << nom << " change son arme et utilise maintenant " << nomNouvelleArme << " (D�g�ts : " << degatsNouvelleArme << ")" << endl;
}

bool Personnage::estVivant()
{
    if(vie)
    {
        cout << nom << " est vivant !" << endl;
    }
    else
    {
        cout << nom << " est mort ! "  << endl;
    }

    return vie > 0;
}

void Personnage::afficherEtat()
{
    cout << "Nom : " << nom << endl;
    cout << "Vie : " << vie << endl;
    cout << "Mana : " << mana << endl;
    armePersonnage.afficher();
}

string Personnage::getNom()
{
    return nom;
}

void Personnage::attaqueMagique(Personnage &cible)
{
    cout << nom << " attaque " << cible.getNom() << " avec un sort." << endl;
    mana -= 30;
    if (mana < 0) //Pour �viter d'avoir une vie n�gative
    {
        mana = 0; //On met la vie � 0 (cela veut dire mort)
    }
    cible.recevoirDegats(10);
}
